import {NavParams, Platform, ViewController} from "ionic-angular";
import {Component} from "@angular/core";


@Component({
  selector: 'modal',
  templateUrl: 'modal.html'
})
export class ModalComponent {
  video;
  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    var videos: any[] = [
      {
        title: 'video 1',
        video: './assets/videos/Charlie Puth - Attention [Official Video].mp4',
        screen: './assets/imgs/V_222012.jpg',
      },

    ];
    this.video = videos[this.params.get('charNum')];
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
