import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ModalComponent } from "../../components/modal/modal";
import { ModalPage } from '../modal/modal';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  character;
  public play = false;
  public screen_button = true;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,
    public modalCtrl: ModalController) { }

  presentLoading() {
    this.screen_button = false;
    this.play = true;
  }

  // openModal(characterNum) {

  //   let modal = this.modalCtrl.create(ModalComponent, characterNum);
  //   modal.present();
  // }
  openModal() { 
    var modalPage = this.modalCtrl.create('ModalPage');
   modalPage.present(); }




}






